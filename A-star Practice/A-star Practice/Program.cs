﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace A_star_Practice
{
    class Program
    {
        Node[] _directions = null;
        Dictionary<string,Node> closed = new Dictionary<string, Node>();

        public Program() {
            _directions = new Node[4];
            _directions[0] = new Node(0, 1);
            _directions[1] = new Node(0, -1);
            _directions[2] = new Node(1, 0);
            _directions[3] = new Node(-1, 0);
        }

        static void Main(string[] args)
        {
            Program p = new Program();

            while (1 == 1) {
                p.run(p);
                Thread.Sleep(1000);
            }
        }

        void run(Program p)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            int[,] grid = p.makeRandomGrid(120, 29, 1000);
            Node start = new Node(0, 0);
            Node end = new Node(grid.GetLength(0) - 1, grid.GetLength(1) - 1);
            Node[] path = p.findPath(grid, start, end);

            for (int x = 0; x <= end.x; ++x)
            {
                for (int y = 0; y <= end.y; ++y)
                {
                    switch (grid[x, y])
                    {
                        case 0: Console.BackgroundColor = ConsoleColor.White; break;
                        case 1: Console.BackgroundColor = ConsoleColor.Black; break;
                    }
                    Console.SetCursorPosition(x, y);
                    Console.Write(" ");
                }
            }

            Node[] closedList = closed.Values.ToArray();
            Console.BackgroundColor = ConsoleColor.DarkYellow;
            foreach (var n in closedList)
            {
                Console.SetCursorPosition(n.x, n.y);
                Console.Write(" ");
            }

            drawEndPoints(start, end);

            if (path == null)
            {
                Console.SetCursorPosition(0, end.y+1);
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write("No path");
                Thread.Sleep(1000);
                return;
            }

            Console.BackgroundColor = ConsoleColor.Red;
            foreach (var n in path)
            {
                Console.SetCursorPosition(n.x, n.y);
                Console.Write(" ");
                Thread.Sleep(50);
            }

            drawEndPoints(start, end);
        }

        private static void drawEndPoints(Node start, Node end)
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(start.x, start.y);
            Console.Write(" ");

            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(end.x, end.y);
            Console.Write(" ");
        }

        Node[] findPath(int[,] grid,Node a,Node b) {
            closed.Clear();
            Dictionary<string, Node> open = new Dictionary<string, Node>();
            Dictionary<string, Node> parents = new Dictionary<string, Node>();
            Dictionary<string, double> hScore = new Dictionary<string, double>();
            Dictionary<string, double> fScore = new Dictionary<string, double>();

            open[a.s] = a;
            parents[a.s] = null;
            hScore[a.s] = 0;
            fScore[a.s] = h(a, b);

            while (open.Count > 0)
            {
                Node n = getNext(open, fScore);
                if (n.s == b.s)
                    return backtrack(parents, n);
                open.Remove(n.s);
                closed.Add(n.s,n);
                foreach (Node d in _directions) {
                    Node next = new Node(n.x + d.x, n.y + d.y);
                    if (oob(next, grid) || closed.ContainsKey(next.s))
                        continue;
                    double score = hScore[n.s] + 1;
                    if (!open.ContainsKey(next.s))
                        open[next.s] = next;
                    else if (score >= hScore[next.s])
                        continue;
                    parents[next.s] = n;
                    hScore[next.s] = score;
                    fScore[next.s] = score + h(next, b);
                }
            }

            return null;
        }

        Node[] backtrack(Dictionary<string, Node> parents, Node n) {
            List<Node> path = new List<Node>();
            path.Add(n);
            while (parents[n.s] != null) {
                path.Insert(0, parents[n.s]);
                n = parents[n.s];
            }
            return path.ToArray();
        }

        bool oob(Node n, int[,] grid) {
            return 
                n.x < 0 ||
                n.y < 0 ||
                n.x >= grid.GetLength(0) ||
                n.y >= grid.GetLength(1) ||
                grid[n.x, n.y] != 0;
        }

        Node getNext(Dictionary<string, Node> open, Dictionary<string, double> fScore) {
            double min = double.MaxValue;
            Node n = null;
            foreach (string k in open.Keys) {
                double f = fScore[k];
                if (f < min) {
                    min = f;
                    n = open[k];
                }
            }
            return n;
        }

        double h(Node a, Node b) {
            int x = a.x - b.x;
            int y = a.y - b.y;
            return Math.Sqrt(x * x + y * y);
        }
        
        int[,] makeGrid(int w,int h) {
            int[,] grid = new int[w, h];
            for (int i = 2; i < w; ++i)
                grid[i, 4] = 1;
            return grid;
        }

        int[,] makeRandomGrid(int w, int h,int blocks)
        {
            int[,] grid = new int[w, h];
            Random random = new Random();
            for (int i=0;i< blocks; ++i) {
                int x = random.Next(w);
                int y = random.Next(h);
                if ((x == w-1 && y == h-1) || (x == 0 && y == 0))
                    continue;
                grid[x, y] = 1;
            }
            return grid;
        }
    }

    class Node
    {
        public int x, y;
        public Node(int x_, int y_)
        {
            x = x_;
            y = y_;
            s = x + "," + y;
        }
        public string s;
    }
}
